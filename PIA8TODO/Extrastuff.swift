//
//  Extrastuff.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-10-09.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func massaPlus() -> String
    {
        return "++++"+self+"++++"
    }
}

extension UIView
{
    func makeMeGreen()
    {
        self.backgroundColor = UIColor.green
    }
}

extension UIImageView
{
    func makeMeRound()
    {
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
    }
}

extension UIViewController
{
    var fancyGreen : UIColor { return UIColor(red: 0.1, green: 0.3, blue: 0.2, alpha: 1) }
    
    func billColor() -> UIColor
    {
        return UIColor(red: 0.1, green: 0.3, blue: 0.2, alpha: 1)
    }
}
