//
//  ItemDetailViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-25.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class ItemDetailViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var itemNameTextfield: UITextField!
    @IBOutlet weak var itemImageview: UIImageView!
    
    
    @IBOutlet weak var difficultyButton1: UIButton!
    
    @IBOutlet weak var difficultyButton2: UIButton!
    
    @IBOutlet weak var difficultyButton3: UIButton!
    
    @IBOutlet weak var difficultyButton4: UIButton!
    
    @IBOutlet weak var difficultyButton5: UIButton!
    
    var currentItem = TODOItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = currentItem.itemname
        
        itemNameTextfield.text = currentItem.itemname
        
        currentItem.downloadImage() {(result : UIImage?) in
            if let itemimage = result {
                self.itemImageview.image = itemimage
            } else {
                self.itemImageview.backgroundColor = UIColor.red
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showDifficulty(difficultyNumber: currentItem.difficulty)
    }
    
    
    @IBAction func saveName(_ sender: Any) {
        currentItem.itemname = itemNameTextfield.text!
        currentItem.save()
    }
    

    @IBAction func chooseImage(_ sender: Any) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func takePicture(_ sender: Any) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let pickedImage = info[.originalImage] as! UIImage
        
        itemImageview.image = pickedImage
        
        self.dismiss(animated: true, completion: nil)
        
        currentItem.uploadImage(itemimage: pickedImage) {(result:Bool) in
            if(result == true)
            {
                print("YEY. UPLOAD OK")
            } else {
                print("NEY. UPLOAD FAIL")
            }
        }
    }
    
    
    @IBAction func clickDifficulty(_ sender: UIButton) {
        
        showDifficulty(difficultyNumber: sender.tag)
        
        currentItem.difficulty = sender.tag
        currentItem.save()
    }
    
    func showDifficulty(difficultyNumber : Int)
    {
        difficultyButton1.backgroundColor = UIColor(named: "difficultyColor")
        difficultyButton2.backgroundColor = UIColor.lightGray
        difficultyButton3.backgroundColor = UIColor.lightGray
        difficultyButton4.backgroundColor = UIColor.lightGray
        difficultyButton5.backgroundColor = UIColor.lightGray
        
        if(difficultyNumber > 1)
        {
            difficultyButton2.backgroundColor = UIColor(named: "difficultyColor")
        }
        if(difficultyNumber > 2)
        {
            difficultyButton3.backgroundColor = UIColor(named: "difficultyColor")
        }
        if(difficultyNumber > 3)
        {
            difficultyButton4.backgroundColor = UIColor(named: "difficultyColor")
        }
        if(difficultyNumber > 4)
        {
            difficultyButton5.backgroundColor = UIColor(named: "difficultyColor")
        }
    }
}
