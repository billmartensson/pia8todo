//
//  ListDetailViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-18.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class ListDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var listItemTextfield: UITextField!
    @IBOutlet weak var listTableview: UITableView!
    
    var currentList = TODOList()

    var ref: DatabaseReference!
    
    var items = [TODOItem]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
        
        listNameLabel.text = currentList.listname
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadListItems()
    }
    
    @IBAction func addListItem(_ sender: Any) {
        
        let theNewList : [String : Any] = ["itemname": listItemTextfield.text]
        
        var fb_lists = ref.child("todo")
        
        if let listowner = currentList.owner
        {
            fb_lists = fb_lists.child(listowner)
        } else {
            fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        }
        
        fb_lists = fb_lists.child("lists")
        fb_lists = fb_lists.child(currentList.key)
        fb_lists = fb_lists.child("listitems")
        fb_lists = fb_lists.childByAutoId()

        fb_lists.setValue(theNewList)
        
        listItemTextfield.text = ""
        loadListItems()
    }
    
    func loadListItems()
    {
        items.removeAll()

        var fb_lists = ref.child("todo")
        if let listowner = currentList.owner
        {
            fb_lists = fb_lists.child(listowner)
        } else {
            fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        }
        fb_lists = fb_lists.child("lists")
        fb_lists = fb_lists.child(currentList.key)
        fb_lists = fb_lists.child("listitems")

        fb_lists.observeSingleEvent(of: .value, with: { (snapshot) in
            
            for listthing in snapshot.children.allObjects as! [DataSnapshot]
            {
                let aitem = TODOItem()
                aitem.loadItem(list: self.currentList, iteminfo: listthing)
                self.items.append(aitem)
            }
            self.listTableview.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listitem") as! ListItemTableViewCell
        
        cell.itemNameLabel.text = items[indexPath.row].makeItStar()
        
        cell.itemDoneView.layer.cornerRadius = cell.itemDoneView.frame.width/2
        
        if(items[indexPath.row].done == true)
        {
            cell.itemDoneView.isHidden = false
        } else {
            cell.itemDoneView.isHidden = true
        }
        
        cell.itemImageview.makeMeRound()
        
        items[indexPath.row].downloadImage() {(result:UIImage?) in
            cell.itemImageview.image = result
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        items[indexPath.row].changeDone()
        items[indexPath.row].save()
        
        loadListItems()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction  = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            self.performSegue(withIdentifier: "itemdetail", sender: indexPath.row)
        }
        let deleteAction  = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
        }
        //shareAction.backgroundColor = UIColor.green
        return [editAction,deleteAction]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! ItemDetailViewController
        
        dest.currentItem = items[sender as! Int]
    }
    
}
