//
//  ListItemTableViewCell.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-18.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit

class ListItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemImageview: UIImageView!
    
    @IBOutlet weak var itemNameLabel: UILabel!
    
    @IBOutlet weak var itemDoneView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
