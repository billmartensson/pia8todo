//
//  ListeditViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-10-25.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class ListeditViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var listnameTextfield: UITextField!
    @IBOutlet weak var inviteTextfield: UITextField!
    @IBOutlet weak var inviteTableview: UITableView!
    
    var currentlist = TODOList()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        listnameTextfield.text = currentlist.listname
    }
    
    
    @IBAction func saveList(_ sender: Any) {
        currentlist.listname = listnameTextfield.text!
        currentlist.save()
    }
    
    @IBAction func inviteUser(_ sender: Any) {
        
        var ref = Database.database().reference()
        
        var emailquery = ref.child("todo").queryOrdered(byChild: "email")
        emailquery = emailquery.queryEqual(toValue: inviteTextfield.text!)
        
        emailquery.observeSingleEvent(of: .value, with: { (snapshot) in
            
            print("GOT RESULT")
            
            for userinfo in snapshot.children.allObjects as! [DataSnapshot]
            {
                let userinfoDict = userinfo.value as! NSDictionary
                
                print(userinfo.key)
                print(userinfoDict)
                
                // SAVE INVITE
                var fbinvite = ref.child("todo").child(Auth.auth().currentUser!.uid)
                fbinvite = fbinvite.child("lists").child(self.currentlist.key)
                fbinvite = fbinvite.child("invites").childByAutoId()
                
                let stuffToSave : [String: Any] = ["email": userinfoDict.value(forKey: "email") as! String, "uid": userinfo.key]
                
                fbinvite.updateChildValues(stuffToSave)
                
                // ADD LIST TO INVITED USER
                var addlist = ref.child("todo").child(userinfo.key)
                addlist = addlist.child("lists").child(self.currentlist.key)
                
                let addListSave : [String: Any] = ["owner": Auth.auth().currentUser!.uid, "listname": self.currentlist.listname]
                
                addlist.updateChildValues(addListSave)
                
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
            let alert = UIAlertController(title: "Error", message: "Could not load", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "invite")
        
        cell?.textLabel?.text = "HEPP"
        
        return cell!
    }
    
}
