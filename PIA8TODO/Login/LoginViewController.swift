//
//  LoginViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-18.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordtextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Messaging.messaging().subscribe(toTopic: "fruit") { error in
            print("Subscribed to news topic")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    

    @IBAction func signupUser(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordtextfield.text!) { (authResult, error) in
            
            if let theError = error
            {
                // AJDÅ ERROR
                print(theError)
                
                let alert = UIAlertController(title: "Error", message: "Signup error", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                
            } else {
                print("TJOHOO SIGN UP OK")
                self.dismiss(animated: false, completion: nil)
            }
            
        }
    }
    

    @IBAction func loginUser(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: emailTextfield.text!, password: passwordtextfield.text!) { (user, error) in
            if let theError = error
            {
                // AJDÅ ERROR
                print(theError)
                let alert = UIAlertController(title: "Error", message: "Login error", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                
            } else {
                print("TJOHOO LOGIN OK")
                self.dismiss(animated: false, completion: nil)
            }
        }
        
    }
    
    
    
    @IBAction func facebookLogin(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_photos"], from: self) { (result, error) -> Void in
            
            if(error != nil)
            {
                print("LOGIN ERROR")
            } else {
                print("LOGIN OK")
                
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                    if let error = error {
                        // ...
                        print(error.localizedDescription)
                        return
                    }
                    // User is signed in
                    print("TJOHOO FB LOGIN OK")
                    self.dismiss(animated: false, completion: nil)
                }
            }
            
        }

    }
    
}
