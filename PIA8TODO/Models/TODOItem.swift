//
//  TODOItem.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-27.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import Foundation
import Firebase

class TODOItem {
    var key = ""
    
    var itemname = ""
    
    var done = false
    
    var difficulty = 3
    
    var myList = TODOList()
    
    func makeItStar() -> String
    {
        // BANAN
        // * BANAN *
        
        return "* "+itemname+" *"
    }
    
    func loadItem(list: TODOList, iteminfo : DataSnapshot)
    {
        myList = list
        
        let listDict = iteminfo.value as! NSDictionary
        
        key = iteminfo.key
        
        itemname = listDict["itemname"] as! String
        
        if let listDone = listDict["done"] as? Bool
        {
            done = listDone
        } else {
            done = false
        }
        
        if let listDifficulty = listDict["difficulty"] as? Int
        {
            difficulty = listDifficulty
        } else {
            difficulty = 3
        }
        
    }
    
    func downloadImage(completion: @escaping (UIImage?) -> Void)
    {
        let storage = Storage.storage()
        
        let storageRef = storage.reference()
        
        let itemFile = storageRef.child("todo/"+key )
        
        itemFile.getData(maxSize: 100 * 1024 * 1024) { data, error in
            if let error = error {
                // Uh-oh, an error occurred!
                print("DOWNLOAD ERROR")
                completion(nil)
            } else {
                // Data for "images/island.jpg" is returned
                let downloadedimage = UIImage(data: data!)
                completion(downloadedimage!)
            }
        }
    }
    
    func uploadImage(itemimage : UIImage, completion: @escaping (Bool) -> Void)
    {
        let storage = Storage.storage()
        
        let storageRef = storage.reference()
        
        let itemFile = storageRef.child("todo/"+key )
        
        let smallerImage = resizeImage(image: itemimage, targetSize: CGSize(width: 500, height: 500))
        
        let pngFile = smallerImage.pngData()!
        
        itemFile.putData(pngFile, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                print("UPLOAD ERROR")
                completion(false)
                return
            }
            print("UPLOAD OK")
            completion(true)
        }
    }
    
    
    func save()
    {
        var ref = Database.database().reference()
        
        var fb_lists = ref.child("todo")
        
        if let listowner = self.myList.owner
        {
            fb_lists = fb_lists.child(listowner)
        } else {
            fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        }
        
        fb_lists = fb_lists.child("lists")
        fb_lists = fb_lists.child(myList.key)
        fb_lists = fb_lists.child("listitems")
        fb_lists = fb_lists.child(key)
        
        let stuffToSave : [String: Any] = ["itemname": itemname, "done": done, "difficulty": difficulty]
        
        fb_lists.updateChildValues(stuffToSave)
    }
    
    func changeDone()
    {
        if(done == true)
        {
            done = false
        } else {
            done = true
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
