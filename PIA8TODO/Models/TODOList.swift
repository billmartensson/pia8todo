//
//  TODOList.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-27.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import Foundation
import Firebase

class TODOList {
    var key = ""

    var listname = ""
    
    var owner : String?
    
    func loadList(listinfo : DataSnapshot)
    {
        let listDict = listinfo.value as! NSDictionary
        
        key = listinfo.key
        
        listname = listDict["listname"] as! String
        
        if let listowner = listDict["owner"] as? String
        {
            self.owner = listowner
        }
        
    }
    
    
    func save()
    {
        var ref = Database.database().reference()
        
        var fb_lists = ref.child("todo")
        fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        fb_lists = fb_lists.child("lists")
        fb_lists = fb_lists.child(key)
        
        let stuffToSave : [String: Any] = ["listname": listname]
        
        fb_lists.updateChildValues(stuffToSave)
    }
}
