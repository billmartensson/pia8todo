//
//  ViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-11.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    
    @IBOutlet weak var theTextfield: UITextField!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        ref = Database.database().reference()
        
        /*
        // SPARA DATA
        ref.child("firstname").setValue("Bill")
        
        ref.child("people").child("someone").child("fullname").setValue("Torsten Torstensson")
        ref.child("people").child("someone").child("address").setValue("Torstengatan 99")

        ref.child("people").child("otherperson").child("fullname").setValue("Torsten Torstensson")
        
        // HÄMTA DATA
        ref.child("people").child("someone").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            
            let thename = value?["fullname"] as? String
            
            print(thename)
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        */
        
        ref.child("fancytext").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let theTextWeGot = snapshot.value as? String
            
            self.theTextfield.text = theTextWeGot
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        
    }

    
    @IBAction func letsSave(_ sender: Any) {
        
        ref.child("fancytext").setValue(theTextfield.text)
        
    }
    
    
    

}

