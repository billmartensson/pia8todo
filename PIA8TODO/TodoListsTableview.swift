//
//  TodoListsTableview.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-10-09.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import Foundation
import UIKit

/*
 
 * todo
 ** USERUID         email
 *** lists
 **** LISTID
 ***** listitems
 
 
 
 * TODO
 ** $uid
 *** lists
 **** $listid
 
 rule write: $uid === auth.uid || $listid/invite/$invitekey/$inviteuser == auth.uid
 
 
 
 
 
 */



extension TodoListsViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "list")
        
        cell?.textLabel?.text = lists[indexPath.row].listname
        //cell?.backgroundColor = self.fancyGreen
        
        if(lists[indexPath.row].owner == nil)
        {
            cell?.backgroundColor = UIColor.white
        } else {
            cell?.backgroundColor = UIColor.yellow
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "listDetail", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction  = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            self.performSegue(withIdentifier: "listedit", sender: indexPath.row)
        }
        let deleteAction  = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
        }
        //shareAction.backgroundColor = UIColor.green
        return [editAction,deleteAction]
    }
}
