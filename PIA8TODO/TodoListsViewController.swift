//
//  TodoListsViewController.swift
//  PIA8TODO
//
//  Created by Bill Martensson on 2018-09-11.
//  Copyright © 2018 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class TodoListsViewController: UIViewController {
    
    // ÄNDRA I MASTER PUBLICERAD VERSION
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var listnameTextfield: UITextField!
    
    @IBOutlet weak var listsTableview: UITableView!
    
    var ref: DatabaseReference!
    
    var lists = [TODOList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
        
        loadingView.isHidden = false
        
        
        print("Tjena".massaPlus())
        
        if(Auth.auth().currentUser != nil)
        {
            var fb_lists = ref.child("todo")
            fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
            fb_lists.updateChildValues(["email": Auth.auth().currentUser!.email!])
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(Auth.auth().currentUser == nil)
        {
            performSegue(withIdentifier: "login", sender: nil)
        } else {
            print(Auth.auth().currentUser?.email)
            self.title = Auth.auth().currentUser?.email
            loadLists()
        }
    }
    
    
    @IBAction func logoutUser(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            // LOGGA UT GICK BRA. KÖR MER KOD.
            performSegue(withIdentifier: "login", sender: nil)
        } catch {
            // GICK INTE ATT LOGGA UT
        }
    }
    
    
    
    func loadLists()
    {
        lists.removeAll()
        
        var fb_lists = ref.child("todo")
        fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        fb_lists = fb_lists.child("lists")
        
        fb_lists.observeSingleEvent(of: .value, with: { (snapshot) in
            
            for listthing in snapshot.children.allObjects as! [DataSnapshot]
            {
                let alist = TODOList()
                alist.loadList(listinfo: listthing)
                self.lists.append(alist)
            }
            self.listsTableview.reloadData()
            self.loadingView.isHidden = true
        }) { (error) in
            print(error.localizedDescription)
            let alert = UIAlertController(title: "Error", message: "Could not load", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    

    @IBAction func addList(_ sender: Any) {
        let theNewList : [String : Any] = ["listname": listnameTextfield.text]
        
        var fb_lists = ref.child("todo")
        fb_lists = fb_lists.child(Auth.auth().currentUser!.uid)
        fb_lists = fb_lists.child("lists")
        
        
        fb_lists.childByAutoId().setValue(theNewList)
        
        loadLists()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "login")
        {
            
        }
        
        if(segue.identifier == "listDetail")
        {
            let dest = segue.destination as! ListDetailViewController
            
            dest.currentList = lists[sender as! Int]
        }
        
        if(segue.identifier == "listedit")
        {
            let dest = segue.destination as! ListeditViewController
            dest.currentlist = lists[sender as! Int]
        }
    }
}
